import React, { Component } from 'react';
import classes from './Layout.module.css';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';


class Layout extends Component {
    state = {
        showSideDrawer: false
    }

    toggle = () => {
       /*  let currentState = this.state.showSideDrawer;
        this.setState({showSideDrawer:!currentState}); */
        //Other approach cleaner approach
        this.setState((prevState) => {
            return {showSideDrawer:!prevState.showSideDrawer}
        })
    }
    render() {
        return (
            <React.Fragment>
                <Toolbar toggle = {this.toggle} />
                <SideDrawer open={this.state.showSideDrawer} closed={this.toggle}/>
                <div>Sidebar, Backdrop</div>
                <main className={classes.content}>
                    {this.props.children}
                </main>
            </React.Fragment>
        );
    }
}

export default Layout;