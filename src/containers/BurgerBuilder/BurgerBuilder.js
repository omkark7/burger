import React, { Component } from 'react';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import Loader from '../../UI/Spinner/Spinner';
import axios from 'axios';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandle';

const INGREDIENTS_PRICE = {
    /* salad: 10,
    bacon: 20,
    cheese: 15,
    meat: 25, */
    salad: 0.5,
    bacon: 0.7,
    cheese: 0.4,
    meat: 1.3,
};

class BurgerBuilder extends Component {
    state = {
        ingredients: null,
        totalPrice: 4,
        purchaseable: false, // For order btn (disable true false)
        purchasing: false, //For order btn Clicked and then display the modal,
        loading: false,
        error: false
    }

    componentDidMount() {
        axios.get("https://react-burger-app-8e15e-default-rtdb.firebaseio.com/ingredients.json")
            .then(response => {
                //console.log(response);
                this.setState({ ingredients: response.data })
            })
            .catch(error => {
                console.log('error');
                this.setState({ error: true })
            });
    }

    render() {
        const disabledInfo = { ...this.state.ingredients };
        for (let key in disabledInfo) {
            disabledInfo[key] = disabledInfo[key] <= 0;
        }

        let orderSummary = (this.state.ingredients ? <OrderSummary
            ingredients={this.state.ingredients}
            purchaseCancelled={this.purchaseCancelHandler}
            purchaseContinue={this.purchaseContinueHandler}
            totalPrice={this.state.totalPrice} /> : null);

        if (this.state.loading) {
            orderSummary = <Loader />;
        }

        let burger = this.state.error ? <p>Ingredient cant be loaded.</p> : <Loader />;
        if (this.state.ingredients) {
            burger = (
                <React.Fragment>
                    <Burger
                        ingredients={this.state.ingredients} />

                    <BuildControls
                        handleLess={this.handleLess}
                        handleMore={this.handleMore}
                        disabledInfo={disabledInfo}
                        price={this.state.totalPrice}
                        purchaseable={this.state.purchaseable} // For order btn (disable true false)
                        ordered={this.purchaseHandler} //For order btn Clicked and then display the modal
                    />
                </React.Fragment>
            );
        }

        return (
            <React.Fragment>
                <Modal show={this.state.purchasing} modalClosed={this.purchaseCancelHandler}>
                    {orderSummary}
                </Modal>
                {burger}
            </React.Fragment>
        );
    }

    handleLess = (type) => {
        const oldCount = this.state.ingredients[type];
        const updatedCount = oldCount - 1;

        /*********************** Dont do like this
            cloneIngredients[type] = this.state.ingredients[type] - 1;
        ******************* */

        if (oldCount) {
            const updatedIngredients = { ...this.state.ingredients };
            updatedIngredients[type] = updatedCount;

            const totalPrice = this.state.totalPrice - INGREDIENTS_PRICE[type];

            this.setState({ ingredients: updatedIngredients, totalPrice });

            this.setState({ ingredients: updatedIngredients });

            this.updatePurchaseableState(updatedIngredients);

        }
    }

    handleMore = (type) => {
        const oldCount = this.state.ingredients[type];
        const updatedCount = oldCount + 1;
        const updatedIngredients = { ...this.state.ingredients };
        updatedIngredients[type] = updatedCount;

        const totalPrice = this.state.totalPrice + INGREDIENTS_PRICE[type];

        this.setState({ ingredients: updatedIngredients, totalPrice });

        this.updatePurchaseableState(updatedIngredients);
    }

    updatePurchaseableState(ingredients) {
        //let clonedState = { ...this.state.ingredients };
        let sum = Object.keys(ingredients)
            .map(c => {
                return ingredients[c];
            })
            .reduce((sum, el) => {
                //el is ingredient value
                //sum is constantly updated  
                //0 is starting element
                return sum + el;
            }, 0);

        this.setState({ purchaseable: sum > 0 })

    }

    purchaseHandler = () => {
        this.setState({ purchasing: true });
    }

    purchaseCancelHandler = () => {
        this.setState({ purchasing: false })
    }

    purchaseContinueHandler = () => {
        this.setState({ loading: false, purchasing: false });
        let queryParams = [];
        for (let i in this.state.ingredients) {
            queryParams.push(encodeURIComponent(i) + '=' + encodeURIComponent(this.state.ingredients[i]));
        }
        queryParams.push('price=' + this.state.totalPrice);
        const queryString = queryParams.join('&');
        this.props.history.push({
            pathname: '/checkout',
            search: '?' + queryString
        });
    }
}

export default withErrorHandler(BurgerBuilder, axios);