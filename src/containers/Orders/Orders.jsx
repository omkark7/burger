import React, { Component } from 'react';
import axios from 'axios';
import Order from '../../components/Order/Order';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandle';

class Orders extends Component {
    state = {
        orders: [],
        loading: true
    }
    constructor() {
        super();
        this.setState({ loading: false });
        console.log(this.state.loading);
    }
    componentDidMount() {
        axios.get("https://react-burger-app-8e15e-default-rtdb.firebaseio.com/orders.json")
            .then(response => {
                //console.log('fetchedOrders');
                let fetchedOrders = [];
                for (let key in response.data) {
                    fetchedOrders.push({
                        ...response.data[key],
                        id: key
                    });
                }
                this.setState({ loading: false, orders: fetchedOrders });
                //console.log(this.state.loading);
            })
            .catch(error => {
                //console.log(error);
                this.setState({ loading: false });
            });
    }

    render() {
        return (
            <div>
                {
                    this.state.orders.map(o => {
                        return <Order key={o.id} ingredients={o.ingredients} price={o.price} />
                    })
                }
            </div>
        );
    }
}

export default withErrorHandler(Orders, axios);