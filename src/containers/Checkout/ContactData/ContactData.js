import React, { Component } from 'react';
import Button from '../../../UI/Button/Button';
import classes from './ContactData.module.css';
import axios from 'axios';
import Spinner from '../../../UI/Spinner/Spinner';
import Input from '../../../UI/Input/Input';

class ContactData extends Component {
    state = {
        orderForm: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Name'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched:false
            },
            street: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Street'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched:false
            },
            zipCode: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Zipcode'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 5,
                    maxLenght: 5
                },
                valid: false,
                touched:false
            },
            country: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Country'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched:false
            },
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Email Id'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched:false
            },
            deliveryMethod: {
                elementType: 'select',
                elementConfig: {
                    options: [
                        { value: 'fastest', displayValue: 'Fastest' },
                        { value: 'cheapest', displayValue: 'Cheapest' }
                    ]
                },
                value: '',
                validation:{},
                valid:true
            },
        },
        loading: false,
        formIsValid:false
    }

    render() {

        const formElementArray = [];
        for (let key in this.state.orderForm) {
            formElementArray.push(
                {
                    id: key,
                    config: this.state.orderForm[key]
                }
            );
        }

        let form = (<form onSubmit={this.handleOrder}>
            {
                formElementArray.map(ele => {
                    return (
                        <Input
                            key={ele.id}
                            elementType={ele.config.elementType}
                            elementConfig={ele.config.elementConfig}
                            value={ele.config.value}
                            changed={(event) => this.inputChangedHandler(event, ele.id)}
                            invalid={!ele.config.valid}
                            shouldValidate= {ele.config.validation}
                            touched={ele.config.touched}
                        />
                    )
                })
            }
            <Button btnType="Success" disabled={!this.state.formIsValid} >Order</Button>
        </form>);

        if (this.state.loading) {
            form = (<Spinner />);
        }
        return (
            <div className={classes.ContactData}>
                <h3>Enter your details</h3>
                {form}
            </div>
        );
    }

    handleOrder = (event) => {
        event.preventDefault();
        // this.setState({ loading: true });

        let formData = {};
        for (let formElementIdentifer in this.state.orderForm) {
            formData[formElementIdentifer] = this.state.orderForm[formElementIdentifer].value;
        }

        const order = {
            ingredients: this.props.ingredients,
            price: this.props.price,
            orderData: formData
        };

        axios.post('https://react-burger-app-8e15e-default-rtdb.firebaseio.com/orders.json', order)
            .then(response => {
                this.setState({ loading: false });
                this.props.history.push('/');
                console.log('response');
            })
            .catch(error => {
                this.setState({ loading: false });
                console.error('error');
                alert('Order Fail');
            });
    }

    inputChangedHandler = (event, inputIdentifer) => {
        const updatedOrderForm = { ...this.state.orderForm };
        const updatedOrderFormElement = { ...updatedOrderForm[inputIdentifer] };
        updatedOrderFormElement.value = event.target.value;
        updatedOrderFormElement.valid = this.checkValidity(updatedOrderFormElement.value, updatedOrderFormElement.validation);
        updatedOrderFormElement.touched = true;
        updatedOrderForm[inputIdentifer] = updatedOrderFormElement;

        let formIsValid = true;
        for(let identifier in updatedOrderForm){
            formIsValid = updatedOrderForm[identifier].valid && formIsValid;
        }
       
        this.setState({ orderForm: updatedOrderForm, formIsValid:formIsValid });
    }

    checkValidity(value, rules) {
        let isValid = true;
        if(!rules){
            return true;
        }
        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }
        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid;
        }
        if (rules.maxLenght) {
            isValid = value.length <= rules.maxLenght && isValid;
        }
        return isValid;
    }
}

export default ContactData;