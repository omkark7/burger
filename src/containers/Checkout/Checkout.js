import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import CheckoutSummary from '../../components/Order/CheckoutSummary/CheckoutSummary';
import ContactData from '../Checkout/ContactData/ContactData';

class Checkout extends Component {
    state = {
        ingredients: null,
        totalPrice: 0,
        searchParam: null
    }

    static getDerivedStateFromProps(props, state) {
        if (props.location.search) { //kept for Rahul Sir viewing
            let ingredients = {};
            let price = 0;
            const query = new URLSearchParams(props.location.search);
            for (let param of query.entries()) {
                if (param[0] === 'price') {
                    price = param[1];
                }
                else {
                    //We get data in params like this         ["bacon", "1"]
                    ingredients[param[0]] = + param[1]; //Here + means it converts to interger
                }
            }
            state.ingredients = ingredients;
            state.totalPrice = price;
        }
        return state;
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log('comp did update');
        return true;
    }

    /* componentWillMount() {
        let ingredients = {};
        let price = 0;
        const query = new URLSearchParams(this.props.location.search);
        for (let param of query.entries()) {
            if (param[0] === 'price') {
                price = param[1];
            }
            else {
                //We get data in params like this         ["bacon", "1"]
                ingredients[param[0]] = + param[1]; //Here + means it converts to interger
            }
        }
        this.setState({ ingredients: ingredients, totalPrice: price });
    } */

    render() {
        return (
            <React.Fragment>
                <CheckoutSummary
                    checkOutCancel={this.handleCheckoutCancel}
                    checkoutContinue={this.handleCheckoutContinue}
                    ingredients={this.state.ingredients} />

                <Route path={this.props.match.path + '/contact-data'}
                    render={(props) => <ContactData ingredients={this.state.ingredients} price={this.state.totalPrice} {...props} />} />

            </React.Fragment>

        );
    }
    handleCheckoutCancel = () => {
        this.props.history.goBack();
    }
    handleCheckoutContinue = () => {
        this.props.history.push('/checkout/contact-data');
    }
}

export default Checkout;