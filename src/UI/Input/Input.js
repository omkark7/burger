import React from 'react';
import classes from './Input.module.css';

const Input = (props) => {
    let inputElement = null;

    const inputClasses = [classes.InputElement];
    if(props.invalid && props.shouldValidate && props.touched){
        inputClasses.push(classes.inValid);
    }

    switch (props.elementType) {
        case 'input':
            inputElement = <input
                className={inputClasses.join(' ')}
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed}
            />;
            break;
        case 'textarea':
            inputElement = <textarea
                className={inputClasses.join(' ')}
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed}
            />;
            break;
        case 'select':
            inputElement = (
                <select
                    className={inputClasses.join(' ')}
                    {...props.elementConfig}
                    value={props.value}
                    onChange={props.changed}
                >
                    {
                        props.elementConfig.options.map(opt => {
                            return <option key={opt.value} value={opt.value} > {opt.displayValue} </option>
                        })
                    }
                </select>);
            break;
        default:
            inputElement = <input
                className={inputClasses.join(' ')}
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed} />;
            break;
    }
    return (
        <div className={classes.Input}>
            <label htmlFor={props.label} className={classes.Label} >
                {props.label}
            </label>
            {inputElement}
            {/* <input
                type={props.type}
                name={props.name}
                className={[classes.Input, classes[props.class]].join(' ')}
                placeholder={props.placeholder}
            /> */}
        </div>
    );
}

export default Input;