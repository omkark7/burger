import React from 'react';
import classes from './DrawerToggle.module.css';

const DrawerToggle = (props) => {
    return ( 
        <React.Fragment>
            {/*  <div onClick={props.toggle} >Menu</div> */}
            <div className={classes.DrawerToggle} onClick={props.toggle} >
                <div></div>
                <div></div>
                <div></div>
            </div>
        </React.Fragment>
     );
}
 
export default DrawerToggle;