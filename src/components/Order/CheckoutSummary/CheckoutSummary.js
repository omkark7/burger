import React from 'react';
import Button from '../../../UI/Button/Button';
import Burger from '../../Burger/Burger';
import classes from './CheckoutSummary.module.css';

const CheckoutSummary = (props) => {
    return (
        <div className={classes.CheckoutSummary}>
            <h1>It will definetly taste superb!</h1>
            <div style={{ width: '100%', margin: 'auto' }}>
                <Burger ingredients={props.ingredients} />
            </div>
            <div>
                <Button btnType="Success" clicked={ props.checkoutContinue}> Submit </Button>
                <Button btnType="Danger" clicked={ props.checkOutCancel}> Cancel </Button>
            </div>
        </div>
    );
}


export default CheckoutSummary;