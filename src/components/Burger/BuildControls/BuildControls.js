import React from 'react';
import BuildControl from './BuildControl/BuildControl';
import classes from './BuildControls.module.css';

const controls = [
    { "label": "Salad", "type": "salad" },
    { "label": "Bacon", "type": "bacon" },
    { "label": "Cheese", "type": "cheese" },
    { "label": "Meat", "type": "meat" }
];


const BurgerControls = (props) => {
    return (
        <div className={classes.BuildControls}>
            <p> Total Price: <strong>{props.price.toFixed(2)}</strong> </p>
            {
                controls.map(c => {
                    return (
                        <BuildControl
                            key={c.label}
                            type={c.type}
                            label={c.label}
                            handleLess={props.handleLess}
                            handleMore={props.handleMore}
                            is_disabled={props.disabledInfo[c.type]} />
                    )
                })
            }
            <button
                className={classes.OrderButton}
                disabled={!props.purchaseable}
                onClick={props.ordered} >
                ORDER NOW
            </button>
        </div>
    );
}


export default BurgerControls;