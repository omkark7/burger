import React from 'react';
import classes from './BuildControl.module.css';

const BuildControl = (props) => {
    const { type, label, handleLess, handleMore, is_disabled } = props;
    return (
        <div className={classes.BuildControl} >
            <div className={classes.Label} > {label} </div>
            <button className={classes.Less} onClick={() => handleLess(type)} disabled={is_disabled}> Less </button>
            <button className={classes.More} onClick={() => handleMore(type)} > More </button>
        </div>
    );
}

export default BuildControl;