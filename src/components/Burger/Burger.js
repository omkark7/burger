import React from 'react';
import classes from './Burger.module.css';
import BurgerIngredient from './BurgerIngredient/BurgerIngredient';

const Burger = (props) => {
    const transformedIngredients = Object.entries(props.ingredients);
    /* const transformedIngredients = Object.keys(props.ingredients).map(igKeys => {
        return [...Array(props.ingredients[igKeys])]
    }); */
    let burgerBody = [];

    transformedIngredients.forEach(ti => {
        let type = ti[0];
        for (let n = 0; n < ti[1]; n++) {
            burgerBody.push(
                <BurgerIngredient type={type} key={n + type + ti[1]} />
            );
        }
    });

    if (burgerBody.length === 0) {
        burgerBody = <p>Please start adding some ingredients!</p>
    }
    return (
        <div className={classes.Burger}>
            <BurgerIngredient key="bread-top" type="bread-top" />
            {burgerBody}
            <BurgerIngredient key="bread-bottom" type="bread-bottom" />
        </div>
    );
}

export default Burger;