import React, { Component } from 'react';
import Button from '../../../UI/Button/Button';

class OrderSummary extends Component {

    componentDidUpdate() {
        console.log('[Order Summary ]');
    }
    
    render() {
        const ingredients = Object.keys(this.props.ingredients)
            .map(igKey => {
                return (
                    <li key={igKey}>
                        <span style={{ textTransform: 'capitalize' }}> {igKey} </span>:
                        {this.props.ingredients[igKey]}
                    </li>
                )
            });
        return (
            <React.Fragment>
                <h3>Order Summary</h3>
                <p>A Delicious Burger with ingredients :</p>
                <ul>
                    {ingredients}
                </ul>
                <p>Total Price: <strong>{this.props.totalPrice.toFixed(2)}</strong></p>
                <p>Continue to checkout?</p>
                <Button btnType="Danger" clicked={this.props.purchaseCancelled} > Cancel </Button>
                <Button btnType="Success" clicked={this.props.purchaseContinue} > Continue </Button>
            </React.Fragment>
        );
    }
}

export default OrderSummary;